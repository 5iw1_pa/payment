<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Repository\PaymentRepository;
use App\Service\OfferService;
use App\Service\TokenDecodeService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Stripe\StripeClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
    /**
     * @Route("/", name="create_payment", methods={"POST"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function create(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, TokenDecodeService $tokenDecodeService, ValidatorInterface $validator, OfferService $offerService): Response
    {
        try {
            $payment = $serializer->deserialize($request->getContent(), Payment::class, 'json', ['groups' => 'create_payment']);
            $errors = $validator->validate($payment, null, ['create_payment']);
            if (count($errors))
                return new Response($serializer->serialize($errors, 'json'), 400, ["content-type"=>"application/json"]);

            try{
                $stripe = new StripeClient($_ENV['STRIPE_API_KEY_SECRET']);
                $responseStripe = $stripe->paymentIntents->retrieve($payment->getStripeId());
            }
            catch (\Exception $e)
            {
                return new Response($serializer->serialize($e, 'json'), 400, ["content-type"=>"application/json"]);
            }

            $payment->setAmount($offerService->getAmount());
            $payment->setProfileId($tokenDecodeService->getProfileId());
            $payment->setUserId($tokenDecodeService->getUserId());
            $payment->setDate(new \DateTime("NOW"));
            $payment->setStatus($responseStripe->status);
            $entityManager->persist($payment);
            $entityManager->flush();

            return new Response($serializer->serialize($payment, 'json', ["groups" => ["get_payment", "get_all"]]), 201, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/", name="payment_all", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function all(SerializerInterface $serializer, PaymentRepository $paymentRepository): Response
    {
        try
        {
            $payments = $paymentRepository->findAll();
            return new Response($serializer->serialize($payments, "json", ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/no-succeeded", name="payment_allNotSucceeded", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function allNotSucceeded(SerializerInterface $serializer, PaymentRepository $paymentRepository): Response
    {
        try
        {
            $payments = $paymentRepository->createQueryBuilder('p')
                ->where('p.status != :status')
                ->setParameter('status','succeeded')
                ->getQuery()
                ->getResult();
            return new Response($serializer->serialize($payments, "json", ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user", name="payment_getByUser", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function getByUser(SerializerInterface $serializer, PaymentRepository $paymentRepository, TokenDecodeService $tokenDecodeService): Response
    {
        try
        {
            $payments = $paymentRepository->findBy(["user_id" => $tokenDecodeService->getUserId()]);
            return new Response($serializer->serialize($payments, 'json', ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user/{user_id}", name="payment_getForOneUser", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getForOneUser(Request $request, SerializerInterface $serializer, PaymentRepository $paymentRepository): Response
    {
        try
        {
            $user_id = $request->attributes->get('user_id');
            $payments = $paymentRepository->findBy(["user_id" => $user_id]);
            return new Response($serializer->serialize($payments, 'json', ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile", name="payment_getByProfile", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function getByProfile(SerializerInterface $serializer, PaymentRepository $paymentRepository, TokenDecodeService $tokenDecodeService): Response
    {
        try
        {
            $payments = $paymentRepository->findBy(["profile_id" => $tokenDecodeService->getProfileId()]);
            return new Response($serializer->serialize($payments, 'json', ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile/{profile_id}", name="payment_getForOneProfile", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getForOneProfile(Request $request, SerializerInterface $serializer, PaymentRepository $paymentRepository): Response
    {
        try
        {
            $profile_id = $request->attributes->get('profile_id');
            $payments = $paymentRepository->findBy(["profile_id" => $profile_id]);
            return new Response($serializer->serialize($payments, 'json', ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/create-checkout-session", name="payment_createStripeCheckoutSession", methods={"POST"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function createStripeCheckoutSession(SerializerInterface $serializer, OfferService $offerService): Response
    {
        try
        {
            $routeSuccess = $_ENV['STRIPE_SUCCESS_URL'];
            $routeError = $_ENV['STRIPE_ERROR_URL'];

            $price = $offerService->getAmount();

            Stripe::setApiKey($_ENV['STRIPE_API_KEY_SECRET']);
            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => [[
                    'price_data' => [
                        'currency' => 'eur',
                        'product_data' => [
                            'name' => 'Abonnement Premium',
                        ],
                        'unit_amount' => ($price * 100), // Convert from € to centimes (better to calc price using centimes)
                    ],
                    'quantity' => 1,
                ]],
                'mode' => 'payment',
                'success_url' => $routeSuccess,
                'cancel_url' => $routeError,
            ]);

            return new Response($serializer->serialize($session, "json"),200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="payment_getOne", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function getOne(Request $request, SerializerInterface $serializer, Payment $payment): Response
    {
        $this->denyAccessUnlessGranted("GET_PAYMENT", $payment);
        try
        {
            return new Response($serializer->serialize($payment, 'json', ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="payment_UpdateStatus", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function UpdateStatus(SerializerInterface $serializer, Payment $payment, EntityManager $entityManager): Response
    {
        if($payment->getStatus() == 'succeeded')
            return new Response($serializer->serialize("paiement deja en succes", 'json'), 400, ["content-type"=>"application/json"]);

        try
        {
            try{
                $stripe = new StripeClient($_ENV['STRIPE_API_KEY_SECRET']);
                $responseStripe = $stripe->paymentIntents->retrieve($payment->getStripeId());
            }
            catch (\Exception $e)
            {
                return new Response($serializer->serialize($e, 'json'), 400, ["content-type"=>"application/json"]);
            }

            $payment->setStatus($responseStripe->status);
            $entityManager->flush();
            return new Response($serializer->serialize($payment, 'json', ["groups" => ["get_payment", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }
}
