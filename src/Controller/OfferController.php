<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Repository\OfferRepository;
use App\Service\Enumeration\StatusOrganisationEnumeration;
use App\Service\OfferService;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/offer")
 */
class OfferController extends AbstractController
{
    /**
     * @Route("/", name="offer_index", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function index(SerializerInterface $serializer,OfferRepository $offerRepository): Response
    {
        try
        {
            $offers = $offerRepository->findAll();
            return new Response($serializer->serialize($offers, 'json'), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/enterprise", name="offer_updateEnterpriseAmount", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateEnterpriseAmount(Request $request,SerializerInterface $serializer, OfferService $offerService): Response
    {
        try
        {
            $offerAmount = $serializer->deserialize($request->getContent(), Offer::class, 'json', ['groups' => 'update_offer']);
            return $offerService->updateOffer(StatusOrganisationEnumeration::ENTERPRISE, $offerAmount->getAmount());
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/freelance", name="offer_updateFreelanceAmount", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateFreelanceAmount(Request $request,SerializerInterface $serializer, OfferService $offerService): Response
    {
        try
        {
            $offerAmount = $serializer->deserialize($request->getContent(), Offer::class, 'json', ['groups' => 'update_offer']);
            return $offerService->updateOffer(StatusOrganisationEnumeration::FREELANCE, $offerAmount->getAmount());
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }
}
