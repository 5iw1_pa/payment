<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Refund;
use App\Repository\RefundRepository;
use App\Service\Enumeration\RefundEnumeration;
use App\Service\TokenDecodeService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Stripe\StripeClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/refund")
 */
class RefundController extends AbstractController
{
    /**
     * @Route("/", name="get_all_refund", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAll(SerializerInterface $serializer, RefundRepository $refundRepository): Response
    {
        try
        {
            $refunds = $refundRepository->findAll();
            return new Response($serializer->serialize($refunds, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/create/{id}", name="create_refund", methods={"POST"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function create(EntityManagerInterface $entityManager, SerializerInterface $serializer, TokenDecodeService $tokenDecodeService, Payment $payment): Response
    {
        $this->denyAccessUnlessGranted("GET_PAYMENT", $payment);
        try
        {
            $refund = new Refund();
            $refund->setPaymentId($payment);
            $refund->setStatus(RefundEnumeration::WAITING);
            $refund->setRequestDate(new DateTime("NOW"));
            $refund->setUserId($tokenDecodeService->getUserId());
            $refund->setProfileId($tokenDecodeService->getProfileId());
            $entityManager->persist($refund);
            $entityManager->flush();

            return new Response($serializer->serialize($refund, 'json', ["groups" => ["get_refund", "get_all"]]), 201, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user", name="refund_getByUser", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function getByUser(SerializerInterface $serializer, RefundRepository $refundRepository, TokenDecodeService $tokenDecodeService): Response
    {
        try
        {
            $refunds = $refundRepository->findBy(["user_id" => $tokenDecodeService->getUserId()]);
            return new Response($serializer->serialize($refunds, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user/{user_id}", name="refund_getForOneUser", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getForOneUser(Request $request, SerializerInterface $serializer, RefundRepository $refundRepository): Response
    {
        try
        {
            $user_id = $request->attributes->get('user_id');
            $refunds = $refundRepository->findBy(["user_id" => $user_id]);
            return new Response($serializer->serialize($refunds, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile", name="refund_getByProfile", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function getByProfile(SerializerInterface $serializer, RefundRepository $refundRepository, TokenDecodeService $tokenDecodeService): Response
    {
        try
        {
            $refunds = $refundRepository->findBy(["profile_id" => $tokenDecodeService->getProfileId()]);
            return new Response($serializer->serialize($refunds, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile/refunded", name="refund_getRefundedByProfile", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PAYMENT")
     */
    public function refund_getRefundedByProfile(SerializerInterface $serializer, RefundRepository $refundRepository, TokenDecodeService $tokenDecodeService): Response
    {
        try
        {
            $refunds = $refundRepository->findBy(["profile_id" => $tokenDecodeService->getProfileId(), "status" => RefundEnumeration::REFUNDED]);
            return new Response($serializer->serialize($refunds, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile/{profile_id}", name="refund_getForOneProfile", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getForOneProfile(Request $request, SerializerInterface $serializer, RefundRepository $refundRepository): Response
    {
        try
        {
            $profileId = $request->attributes->get('profile_id');
            $refunds = $refundRepository->findBy(["profile_id" => $profileId]);
            return new Response($serializer->serialize($refunds, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/refunded/{id}", name="refund_refunded", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function refunded(Refund $refund, EntityManagerInterface $entityManager, SerializerInterface $serializer): Response
    {
        try
        {
            try
            {
                $stripe = new StripeClient($_ENV['STRIPE_API_KEY_SECRET']);
                $stripe->refunds->create(['payment_intent' => $refund->getPaymentId()->getStripeId()]);
            }
            catch (\Exception $e)
            {
                return new Response($serializer->serialize($e, 'json'), 500, ["content-type"=>"application/json"]);
            }
            $refund->setStatus(RefundEnumeration::REFUNDED);
            $entityManager->flush();
            return new Response($serializer->serialize($refund, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/refused/{id}", name="refund_refused", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function refused(Refund $refund, EntityManagerInterface $entityManager, SerializerInterface $serializer): Response
    {
        try
        {
            $refund->setStatus(RefundEnumeration::REFUSED);
            $entityManager->flush();
            return new Response($serializer->serialize($refund, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/cancel/{id}", name="refund_cancel", methods={"PUT"})
     */
    public function cancel(Refund $refund, EntityManagerInterface $entityManager, SerializerInterface $serializer): Response
    {
        $this->denyAccessUnlessGranted("CANCEL_REFUND", $refund);
        try
        {
            $refund->setStatus(RefundEnumeration::CANCEL);
            $entityManager->flush();
            return new Response($serializer->serialize($refund, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="get_one_refund", methods={"GET"})
     */
    public function getOne(SerializerInterface $serializer, Refund $refund): Response
    {
        $this->denyAccessUnlessGranted("GET_REFUND", $refund);
        try
        {
            return new Response($serializer->serialize($refund, 'json', ["groups" => ["get_refund", "get_all"]]), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }
}
