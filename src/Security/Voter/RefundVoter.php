<?php

namespace App\Security\Voter;

use App\Service\TokenDecodeService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class RefundVoter extends Voter
{
    public const GET_REFUND = 'GET_REFUND';
    public const CANCEL_REFUND = 'CANCEL_REFUND';
    private $security;
    private $tokenDecodeService;

    public function __construct(Security $security, TokenDecodeService $tokenDecodeService)
    {
        $this->security = $security;
        $this->tokenDecodeService = $tokenDecodeService;
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::GET_REFUND, self::CANCEL_REFUND])
            && $subject instanceof \App\Entity\Refund;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        if($this->security->isGranted("ROLE_ADMIN"))
            return true;

        if($subject->getProfileId() == $this->tokenDecodeService->getProfileId() && $this->security->isGranted("ROLE_ENTERPRISE_ADMIN"))
            return true;

        // ... (check conditions and return true to grant permission) ...
//        switch ($attribute) {
//            case self::GET_REFUND:
//                if($subject->getIdProfile() == $this->tokenDecodeService->getIdProfile() && $this->security->isGranted("ROLE_ENTERPRISE_ADMIN"))
//                    return true;
//                break;
//
//        }

        return false;
    }
}
