<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 * @UniqueEntity("stripe_id",message="Cet id stripe est déja utilisé", groups={"create_payment"})
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_all"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_all"})
     */
    private $user_id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_all"})
     * @Assert\NotBlank(message="Le montant ne peut pas être vide")
     */
    private $amount;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get_all"})
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"create_payment","get_all"})
     * @Assert\NotBlank(message="L'id stripe ne peut pas etre vide", groups="create_payment")
     */
    private $stripe_id;

    /**
     * @ORM\OneToOne(targetEntity=Refund::class, mappedBy="payment_id", cascade={"persist", "remove"})
     * @Groups({"get_payment"})
     */
    private $refund;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_payment"})
     */
    private $profile_id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups({"get_all"})
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStripeId(): ?string
    {
        return $this->stripe_id;
    }

    public function setStripeId(string $stripe_id): self
    {
        $this->stripe_id = $stripe_id;

        return $this;
    }

    public function getRefund(): ?Refund
    {
        return $this->refund;
    }

    public function setRefund(Refund $refund): self
    {
        // set the owning side of the relation if necessary
        if ($refund->getPaymentId() !== $this) {
            $refund->setPaymentId($this);
        }

        $this->refund = $refund;

        return $this;
    }

    public function getProfileId(): ?int
    {
        return $this->profile_id;
    }

    public function setProfileId(int $profile_id): self
    {
        $this->profile_id = $profile_id;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
