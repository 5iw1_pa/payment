<?php

namespace App\Entity;

use App\Repository\RefundRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RefundRepository::class)
 */
class Refund
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_all"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_all"})
     */
    private $user_id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get_all"})
     */
    private $request_date;

    /**
     * @ORM\OneToOne(targetEntity=Payment::class, inversedBy="refund", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get_refund","create_refund"})
     * @Assert\NotBlank(message="L'id du paiement ne peut pas etre vide", groups="create_refund")
     */
    private $payment_id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_all"})
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_refund"})
     */
    private $profile_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getRequestDate(): ?\DateTimeInterface
    {
        return $this->request_date;
    }

    public function setRequestDate(\DateTimeInterface $request_date): self
    {
        $this->request_date = $request_date;

        return $this;
    }

    public function getPaymentId(): ?Payment
    {
        return $this->payment_id;
    }

    public function setPaymentId(Payment $payment_id): self
    {
        $this->payment_id = $payment_id;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProfileId(): ?int
    {
        return $this->profile_id;
    }

    public function setProfileId(int $profile_id): self
    {
        $this->profile_id = $profile_id;

        return $this;
    }
}
