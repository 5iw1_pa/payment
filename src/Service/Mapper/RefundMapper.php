<?php

namespace App\Service\Mapper;

use App\Dto\RefundDto;
use App\Entity\Refund;
use App\Repository\PaymentRepository;
use DateTime;
use Exception;

class RefundMapper
{

  public static function RefundDtoToRefund(RefundDto $refundDto, PaymentRepository $paymentRepository): Refund
  {
    $refund = new Refund();
    $refund->setUserId($refundDto->getUserId());
    $refund->setRequestDate(new DateTime());
    $refund->setStatus("PENDING");
    $payment = $paymentRepository->findOneBy(["id" => $refundDto->getPaymentId()]);
    if ($payment === null)
      throw new Exception("Payment not found!");
    $refund->setPaymentId($payment);
    return $refund;
  }
}