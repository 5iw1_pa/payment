<?php
namespace App\Service\Mapper;

use App\Dto\PaymentDto;
use App\Entity\Payment;
use App\Enum\PaymentType;
use DateTime;

class PaymentMapper
{
    public static function PaymentDtoToPayment(PaymentDto $paymentDto) : Payment
    {
        $payment = new Payment();
        $payment->setUserId($paymentDto->getUserId());
        $payment->setAmount($paymentDto->getAmount());
        $payment->setDate(new DateTime());
        $payment->setStripeId($paymentDto->getStripeId());
        $payment->setType("PAYMENT");
        return $payment;
    }

    public static function PaymentToRefundPayment(Payment $payment): Payment
    {
        $newPayment = new Payment();
        $newPayment->setUserId($payment->getUserId());
        $newPayment->setAmount($payment->getAmount());
        $newPayment->setDate(new DateTime());
        $newPayment->setStripeId($payment->getStripeId());
        $newPayment->setType("REFUND");
        return $newPayment;
    }

    public static function PaymentDtoToRefusedPayment(Payment $payment): Payment
    {
        $newPayment = new Payment();
        $newPayment->setUserId($payment->getUserId());
        $newPayment->setAmount($payment->getAmount());
        $newPayment->setDate(new DateTime());
        $newPayment->setStripeId($payment->getStripeId());
        $newPayment->setType("REFUSED");
        return $newPayment;
    }
}