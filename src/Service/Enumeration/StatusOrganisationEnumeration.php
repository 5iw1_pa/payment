<?php


namespace App\Service\Enumeration;


class StatusOrganisationEnumeration
{
    const ENTERPRISE = 1;
    const FREELANCE = 2;
}