<?php

namespace App\Service\Enumeration;

class RefundEnumeration {
    const WAITING = 0;
    const REFUNDED = 1;
    const REFUSED = 2;
    const CANCEL = 3;
}
