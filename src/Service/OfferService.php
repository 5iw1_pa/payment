<?php

namespace App\Service;

use App\Entity\Offer;
use App\Repository\OfferRepository;
use App\Service\Enumeration\StatusOrganisationEnumeration;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

class OfferService
{
    private $serializer;
    private $offerRepository;
    private $entityManager;
    private $security;

    public function __construct(SerializerInterface $serializer,OfferRepository $offerRepository, EntityManagerInterface $entityManager, Security $security)
    {
        $this->serializer = $serializer;
        $this->offerRepository = $offerRepository;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function updateOffer(int $status, int $amount) : Response
    {
        try
        {
            $offer = $this->offerRepository->findOneBy(['status'=>$status]);
            if(empty($offer))
            {
                $offer = new Offer();
                $offer->setStatus($status);
                $offer->setAmount($amount);
                $this->entityManager->persist($offer);
                $this->entityManager->flush();
                return new Response($this->serializer->serialize($offer, 'json'), 201, ["content-type"=>"application/json"]);
            }
            else
            {
                $offer->setAmount($amount);
                $this->entityManager->flush();
                return new Response($this->serializer->serialize($offer, 'json'), 200, ["content-type"=>"application/json"]);
            }
        }
        catch (\Exception $exception)
        {
            return new Response($this->serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    public function getAmount() : int
    {
        $status = 0;
        if($this->security->isGranted("ROLE_ENTERPRISE_ADMIN"))
            $status = StatusOrganisationEnumeration::ENTERPRISE;
        elseif ($this->security->isGranted("ROLE_FREELANCE"))
            $status = StatusOrganisationEnumeration::FREELANCE;

        $offer = $this->offerRepository->findOneBy(['status'=>$status]);
        if(empty($offer))
            throw new Exception("Aucune offre trouve");

        return $offer->getAmount();

    }
}