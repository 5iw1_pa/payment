<?php

namespace App\Service;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\Security\Core\Security;

class TokenDecodeService
{
    private $JWTEncoder;
    private $security;

    public function __construct(JWTEncoderInterface $JWTEncoder,Security $security)
    {
        $this->JWTEncoder = $JWTEncoder;
        $this->security = $security;
    }

    public function getUserId()
    {
        return $this->JWTEncoder->decode($this->security->getToken()->getCredentials())["id_user"];
    }

    public function getProfileId()
    {
        return $this->JWTEncoder->decode($this->security->getToken()->getCredentials())["id_profile"];
    }
}