<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220626215725 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE payment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE refund_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE payment (id INT NOT NULL, user_id INT NOT NULL, amount INT NOT NULL, date DATE NOT NULL, stripe_id VARCHAR(255) NOT NULL, profile_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE refund (id INT NOT NULL, payment_id_id INT NOT NULL, user_id INT NOT NULL, request_date DATE NOT NULL, status INT NOT NULL, profile_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B2C1458280DA8E5 ON refund (payment_id_id)');
        $this->addSql('ALTER TABLE refund ADD CONSTRAINT FK_5B2C1458280DA8E5 FOREIGN KEY (payment_id_id) REFERENCES payment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE refund DROP CONSTRAINT FK_5B2C1458280DA8E5');
        $this->addSql('DROP SEQUENCE payment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE refund_id_seq CASCADE');
        $this->addSql('DROP TABLE payment');
        $this->addSql('DROP TABLE refund');
    }
}
